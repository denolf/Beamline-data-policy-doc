### From version 4.3.1

1. Backup rules

Modify get_rules.py script by replacing:
```
rules = service.search(sessionId, "Rule INCLUDE Group")
```
by
```
rules = service.search(sessionId, "Rule INCLUDE Grouping")
```

And run:
```
lindemaria:/opt2/upgrade/icat.server-4.3.1/icat % ./get_rules.py  https://ovm-icat.esrf.fr:8181 db username admin password ****
126 rules have been written to rules.authz
```

File looks like:
```
addrule null DatafileParameter<->Datafile<->Dataset<->Investigation<->InvestigationInstrument<->Instrument<->InstrumentScientist<->User[name=:user] R
addrule null Dataset<->Investigation<->InvestigationInstrument<->Instrument<->InstrumentScientist<->User[name=:user] R
addrule null DatasetParameter<->Dataset<->Investigation<->InvestigationInstrument<->Instrument<->InstrumentScientist<->User[name=:user] R
addrule null Investigation<->InvestigationInstrument<->Instrument<->InstrumentScientist<->User[name=:user] R
addrule null InvestigationInstrument<->Instrument<->InstrumentScientist<->User[name=:user] R
```

### From version 4.3.2

1. Configure
2. Install

Check that it works:
```
./testicat https://lindemaria.esrf.fr:8181 esrf username demariaa password ****
```

### From version 4.4.0

1. Configure
2. Install
3. Scripts for upgrading the schema

```
UPDATE INVESTIGATIONUSER SET ROLE = 'member' WHERE ROLE IS NULL; 
ALTER TABLE INVESTIGATIONUSER MODIFY (ROLE varchar2(255) NOT NULL); 
ALTER TABLE INVESTIGATIONUSER DROP CONSTRAINT UNQ_INVESTIGATIONUSER_0;
DROP INDEX UNQ_INVESTIGATIONUSER_0;
ALTER TABLE INVESTIGATIONUSER ADD CONSTRAINT UNQ_INVESTIGATIONUSER_0 UNIQUE (USER_ID, INVESTIGATION_ID, ROLE); 
```

### From version 4.5.0

1. Configure
2. Install
3. Scripts

```
column seq_count new_value next_num;
select seq_count + 51 as seq_count from sequence;
CREATE SEQUENCE SEQ_GEN_SEQUENCE INCREMENT BY 50 START WITH &next_num;
```

Chec if it works:
```
icat.server-4.5.0/icat.server % ./testicat https://lindemaria.esrf.fr:8181 esrf username demariaa password ***
```


