<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<group NX_class="NXentry" groupName="${entry}">
    <title ESRF_description="Name of the scan" ESRF_mandatory="Mandatory" NAPItype="NX_CHAR">${scanName}</title>
    <experiment_identifier ESRF_description="Proposal code" ESRF_mandatory="Mandatory" NAPItype="NX_CHAR">${proposal}</experiment_identifier>
    <start_time ESRF_description="Scan starting date" ESRF_mandatory="Mandatory" NAPItype="NX_DATE_TIME">${startDate}</start_time>
    <end_time ESRF_description="Scan ending date" ESRF_mandatory="Mandatory" NAPItype="NX_DATE_TIME">${endDate}</end_time>
    <duration ESRF_description="Total acquisition time" NAPItype="NX_FLOAT64" NX_units="NX_TIME" units="min">${scanDuration}</duration>
    <collection_time ESRF_description="Exposure time" NAPItype="NX_FLOAT64" NX_units="NX_TIME" units="s">${ccdtime}</collection_time>
    <group NX_class="NXsample" groupName="sample">
        <name ESRF_description="Name of the sample" ESRF_mandatory="Mandatory" NAPItype="NX_CHAR">${sampleName}</name>
        <distance ESRF_description="Source/sample distance" NAPItype="NX_FLOAT64" NX_units="NX_LENGTH" units="mm">${sourceSampleDistance}</distance>
        <matrix ESRF_description="Matrix of sample" NAPItype="NX_CHAR">${sampleMatrix}</matrix>
        <group NX_class="NXpositioner" groupName="positioner">
            <name ESRF_description="Sample motors labels" ESRF_label_for="sample_positions" NAPItype="NX_CHAR">${sample_motors}</name>
            <value ESRF_description="Sample motors positions" ESRF_label="sample_motors" ESRF_mandatory="Mandatory for the group" NAPItype="NX_CHAR" NX_units="NX_ANY">${sample_positions}</value>
        </group>
        <group NX_class="NXsensor" groupName="sensor">
            <name ESRF_description="Sample sensors labels" ESRF_label_for="sample_sensors_values" NAPItype="NX_CHAR">${sample_sensors_labels}</name>
            <value ESRF_description="Sample sensors values" ESRF_label="sample_sensors_labels" ESRF_mandatory="Mandatory for the group" NAPItype="NX_CHAR" NX_units="NX_ANY">${sample_sensors_values}</value>
        </group>
        <pixel_size ESRF_description="Pixel Size" NAPItype="NX_FLOAT64" units="micron">${pixelSize}</pixel_size>
        <focus_position ESRF_description="In focus sample position" NAPItype="NX_FLOAT64" units="mm">${sx0}</focus_position>
        <group NX_class="NXenvironment" groupName="vacuum">
            <group NX_class="NXsensor" groupName="sensor">
                <name ESRF_description="Vacuum labels" ESRF_label_for="vacuum_values" NAPItype="NX_CHAR">${vacuum_labels}</name>
                <value ESRF_description="Vacuum values" ESRF_label="vacuum_labels" NAPItype="NX_CHAR" NX_units="NX_ANY">${vacuum_values}</value>
            </group>
        </group>
    </group>
    <group NX_class="NXinstrument" groupName="instrument">
        <name ESRF_description="ID of the beamline" NAPItype="NX_CHAR">${beamlineID}</name>
        <group NX_class="NXattenuator" groupName="attenuator">
            <attenuators_labels ESRF_description="Attenuators labels" ESRF_label_for="attenuators_positions" NAPItype="NX_CHAR">${attenuators_labels}</attenuators_labels>
            <attenuators_positions ESRF_description="Attenuators positions" ESRF_label="attenuators_labels" ESRF_mandatory="Mandatory for the group" NAPItype="NX_CHAR">${attenuators_positions}</attenuators_positions>
        </group>
        <group NX_class="NXbeam" groupName="beam">
            <height ESRF_description="Beam Height" NAPItype="NX_FLOAT64" units="mm">${beamHeight}</height>
            <width ESRF_description="Beam Width" NAPItype="NX_FLOAT64" units="mm">${beamWidth}</width>
            <micro_beam_size ESRF_description="Microbeam Size" NAPItype="NX_FLOAT64" units="micron">${mbSize}</micro_beam_size>
        </group>
        <group NX_class="NXdetector" groupName="detector">
            <acquisition_mode ESRF_description="CCD mode can be 'FFM' or 'FTM'" NAPItype="NX_CHAR">${ccdMode}</acquisition_mode>
            <name ESRF_description="Camera Name" ESRF_mandatory="Mandatory for the group" NAPItype="NX_CHAR">${cameraName}</name>
            <pixel_size ESRF_description="Detector pixel size" NAPItype="NX_FLOAT64" units="micron">${detectorPixelSize}</pixel_size>
            <distortion_parameters ESRF_description="Detector distortion parameters" NAPItype="NX_CHAR">${detectorDistortionParams}</distortion_parameters>
            <group NX_class="NXsensor" groupName="sensor">
                <name ESRF_description="Detector sensors labels" ESRF_label_for="detector_sensors_values" ESRF_mandatory="Mandatory for the group" NAPItype="NX_CHAR">${detector_sensors_labels}</name>
                <value ESRF_description="Detector sensors values" ESRF_label="detector_sensors_labels" NAPItype="NX_CHAR" NX_units="NX_ANY">${detector_sensors_values}</value>
            </group>
            <group NX_class="NXpositioner" groupName="positioner">
                <name ESRF_description="Detectors motors labels" ESRF_label_for="detectors_positions" ESRF_mandatory="Mandatory for the group" NAPItype="NX_CHAR">${detectors_motors}</name>
                <value ESRF_description="Detectors motors positions" ESRF_label="detectors_motors" NAPItype="NX_CHAR" NX_units="NX_ANY">${detectors_positions}</value>
            </group>
            <sample_distance ESRF_description="Sample/detector distance" NAPItype="NX_FLOAT64" units="mm">${sampleDetectorDistance}</sample_distance>
            <source_distance ESRF_description="Source/detector distance" NAPItype="NX_FLOAT64" units="mm">${sourceDetectorDistance}</source_distance>
            <binning ESRF_description="Camera Binning mode" NAPItype="NX_CHAR">${cameraBinning}</binning>
            <fiber ESRF_description="Camera Optic Fibers" NAPItype="NX_CHAR">${cameraFibers}</fiber>
        </group>
        <group NX_class="NXinsertion_device" groupName="insertion_device">
            <gap ESRF_description="insertionDeviceGap" ESRF_label="insertionDeviceName" ESRF_mandatory="Mandatory for the group" NAPItype="NX_CHAR" NX_units="NX_LENGTH" units="mm">${insertionDeviceGap}</gap>
            <name ESRF_description="insertionDeviceName" ESRF_label_for="insertionDeviceGap" NAPItype="NX_CHAR">${insertionDeviceName}</name>
        </group>
        <group NX_class="NXsource" groupName="source">
            <name ESRF_mandatory="Mandatory">ESRF</name>
            <type ESRF_mandatory="Mandatory">Synchrotron X-ray Source</type>
            <mode ESRF_description="Machine mode" ESRF_mandatory="Mandatory" NAPItype="NX_CHAR">${machineMode}</mode>
            <current_start ESRF_description="Machine current" ESRF_mandatory="Mandatory" NAPItype="NX_FLOAT64" units="mA">${SR_Current}</current_start>
            <current_end ESRF_description="Machine current" ESRF_mandatory="Mandatory" NAPItype="NX_FLOAT64" record="final" units="mA">${SR_Current}</current_end>
        </group>
        <group NX_class="NXslit" groupName="primary_slit">
            <x_gap ESRF_description="Horizontal gap" ESRF_mandatory="Mandatory for the group" NAPItype="NX_FLOAT64" units="mm">${slit_PS_HG}</x_gap>
            <y_gap ESRF_description="Vertical gap" ESRF_mandatory="Mandatory for the group" NAPItype="NX_FLOAT64" units="mm">${slit_PS_VG}</y_gap>
            <x_offset ESRF_description="Horizontal offset" ESRF_mandatory="Mandatory for the group" NAPItype="NX_FLOAT64" units="mm">${slit_PS_HO}</x_offset>
            <y_offset ESRF_description="Vertical offset" ESRF_mandatory="Mandatory for the group" NAPItype="NX_FLOAT64" units="mm">${slit_PS_VO}</y_offset>
        </group>
        <group NX_class="NXslit" groupName="secondary_slit">
            <x_gap ESRF_description="Horizontal gap" ESRF_mandatory="Mandatory for the group" NAPItype="NX_FLOAT64" units="mm">${slit_SS_HG}</x_gap>
            <y_gap ESRF_description="Vertical gap" ESRF_mandatory="Mandatory for the group" NAPItype="NX_FLOAT64" units="mm">${slit_SS_VG}</y_gap>
            <x_offset ESRF_description="Horizontal offset" ESRF_mandatory="Mandatory for the group" NAPItype="NX_FLOAT64" units="mm">${slit_SS_HO}</x_offset>
            <y_offset ESRF_description="Vertical offset" ESRF_mandatory="Mandatory for the group" NAPItype="NX_FLOAT64" units="mm">${slit_SS_VO}</y_offset>
        </group>
        <group NX_class="NXmonochromator" groupName="monochromator">
            <name ESRF_description="Monochromator Name" ESRF_mandatory="Mandatory for the group" NAPItype="NX_CHAR">${monochromatorName}</name>
        </group>
        <group NX_class="NXattenuator" groupName="filter">
            <position ESRF_description="Filters name" ESRF_mandatory="Mandatory for the group" NAPItype="NX_CHAR">${filter}</position>
        </group>
        <group NX_class="NXcollection" groupName="optics">
            <name ESRF_description="Optics Name" ESRF_mandatory="Mandatory for the group" NAPItype="NX_CHAR">${opticsName}</name>
            <distortion_parameters ESRF_description="Optics distortion parameters" NAPItype="NX_CHAR">${opticsDistortionParams}</distortion_parameters>
            <group NX_class="NXpositioner" groupName="positioner">
                <name ESRF_description="Optics motors labels" ESRF_label_for="optics_positions" NAPItype="NX_CHAR">${optics_motors}</name>
                <value ESRF_description="Optics motors positions" ESRF_label="optics_motors" ESRF_mandatory="Mandatory for the group" NAPItype="NX_CHAR" NX_units="NX_ANY">${optics_positions}</value>
            </group>
            <group NX_class="NXsensor" groupName="sensor">
                <name ESRF_description="Optics sensors labels" ESRF_label_for="optics_sensors_values" NAPItype="NX_CHAR">${optics_sensors_labels}</name>
                <value ESRF_description="Optics sensors values" ESRF_label="optics_sensors_labels" ESRF_mandatory="Mandatory for the group" NAPItype="NX_CHAR" NX_units="NX_ANY">${optics_sensors_values}</value>
            </group>
        </group>
    </group>
    <scanType ESRF_description="Scan type can be 'step_by_step' or 'continuous'&#10;&#9;&#9;" NAPItype="NX_CHAR">${scanType}</scanType>
    <scan_number ESRF_description="Scan number" NAPItype="NX_FLOAT64">${SCAN_N}</scan_number>
    <group NX_class="NXcollection" groupName="mrt">
        <multi_slit_type ESRF_description="Multislit Type" NAPItype="NX_CHAR">${mscType}</multi_slit_type>
        <dose_rate ESRF_description="Dose Rate" NAPItype="NX_FLOAT64" units="Gy/s/mA">${doseRate}</dose_rate>
        <ctc_motor ESRF_description="C-to-C Motor" NAPItype="NX_CHAR">${ctcMot}</ctc_motor>
        <ctc_spacing ESRF_description="C-to-C Spacing" NAPItype="NX_FLOAT64" units="micron">${ctcSpacing}</ctc_spacing>
        <ctc_n ESRF_description="Number of Irradiations" NAPItype="NX_FLOAT64">${ctcN}</ctc_n>
        <cross_motor ESRF_description="Crossfiring Motor" NAPItype="NX_CHAR">${crossMot}</cross_motor>
        <cross_angle ESRF_description="Crossfiring Angle" NAPItype="NX_FLOAT64" units="deg">${crossAngle}</cross_angle>
        <cross_n ESRF_description="Number of Crossfiring" NAPItype="NX_FLOAT64">${crossN}</cross_n>
        <intlcd_motor ESRF_description="Interlaced Motor" NAPItype="NX_CHAR">${intlcdMot}</intlcd_motor>
        <intlcd_offset ESRF_description="Interlaced Offset" NAPItype="NX_FLOAT64" units="micron">${intlcdOff}</intlcd_offset>
        <z_start_position ESRF_description="Z Start Position" NAPItype="NX_FLOAT64" units="mm">${gonioz_start}</z_start_position>
        <z_stop_position ESRF_description="Z Stop Position" NAPItype="NX_FLOAT64" units="mm">${gonioz_stop}</z_stop_position>
        <z_speed ESRF_description="Z Last Speed" NAPItype="NX_FLOAT64" units="mm/s">${gonioz_speed}</z_speed>
        <IC01 ESRF_description="Counts on ION chamber 0-1" NAPItype="NX_FLOAT64">${IC01}</IC01>
        <IC02 ESRF_description="Counts on ION chamber 0-2" NAPItype="NX_FLOAT64">${IC02}</IC02>
        <IC0MU1 ESRF_description="Counts on ION MUSST chamber 0-1" NAPItype="NX_FLOAT64">${IC0MU1}</IC0MU1>
        <IC0MU2 ESRF_description="Counts on ION MUSST chamber 0-2" NAPItype="NX_FLOAT64">${IC0MU2}</IC0MU2>
        <IONCH1 ESRF_description="Counts on ION chamber 1" NAPItype="NX_FLOAT64">${IONCH1}</IONCH1>
        <IONCH2 ESRF_description="Counts on ION chamber 2" NAPItype="NX_FLOAT64">${IONCH2}</IONCH2>
    </group>
    <group NX_class="NXcollection" groupName="tomo">
        <tomo_n ESRF_description="Projections NUMERIC" NAPItype="NX_FLOAT64">${tomo_N}</tomo_n>
        <ref_n ESRF_description="Reference images NUMERIC" NAPItype="NX_FLOAT64">${ref_N}</ref_n>
        <dark_n ESRF_description="Dark images NUMERIC" NAPItype="NX_FLOAT64">${dark_N}</dark_n>
        <ref_on ESRF_description="Reference images every REF_ON projections" NAPItype="NX_FLOAT64">${ref_On}</ref_on>
        <y_step ESRF_description="Sample translation for reference images" NAPItype="NX_FLOAT64" units="mm">${y_Step}</y_step>
        <FTOMO_PAR ESRF_description="Ftomo parameters" NAPItype="NX_CHAR">${FTOMO_PAR}</FTOMO_PAR>
    </group>
    <group NX_class="NXcollection" groupName="scan">
        <group NX_class="NXcollection" groupName="axis1">
            <name ESRF_description="1st scan axis" NAPItype="NX_CHAR">${scanAxis_1}</name>
            <range ESRF_description="Scan range along 1st axis" NAPItype="NX_FLOAT64" units="micron">${scanRange_1}</range>
            <dimension ESRF_description="Number of scan points along 1st axis" NAPItype="NX_FLOAT64">${scanDim_1}</dimension>
        </group>
        <group NX_class="NXcollection" groupName="axis2">
            <name ESRF_description="2nd scan axis" NAPItype="NX_CHAR">${scanAxis_2}</name>
            <range ESRF_description="Scan range along 2nd axis" NAPItype="NX_FLOAT64" units="micron">${scanRange_2}</range>
            <dimension ESRF_description="Number of scan points along 2nd axis" NAPItype="NX_FLOAT64">${scanDim_2}</dimension>
        </group>
        <group NX_class="NXcollection" groupName="axis3">
            <name ESRF_description="3rd scan axis" NAPItype="NX_CHAR">${scanAxis_3}</name>
            <range ESRF_description="Scan range along 3rd axis" NAPItype="NX_FLOAT64" units="micron">${scanRange_3}</range>
            <dimension ESRF_description="Number of scan points along 3rd axis" NAPItype="NX_FLOAT64">${scanDim_3}</dimension>
        </group>
        <dwell_time ESRF_description="Dwell time per step" NAPItype="NX_FLOAT64" units="s">${dwellTime}</dwell_time>
    </group>
    <group NX_class="NXcollection" groupName="measurement">
        <group NX_class="NXcollection" groupName="initial">
            <link groupName="SR_Current" ref="/instrument/source/current_start"/>
            <link groupName="vacuum" ref="/sample/vacuum/sensor/value"/>
            <energy ESRF_description="Energy" NAPItype="NX_FLOAT64" units="keV">${energy}</energy>
            <i0 ESRF_description="Incident flux" NAPItype="NX_FLOAT64" units="photons/s">${i0}</i0>
            <it ESRF_description="Transmitted flux" NAPItype="NX_FLOAT64" units="photons/s">${it}</it>
            <iC ESRF_description="Ionisation chamber flux" NAPItype="NX_FLOAT64" units="A">${iC}</iC>
        </group>
        <group NX_class="NXcollection" groupName="final">
            <link groupName="SR_Current" ref="/instrument/source/current_end"/>
            <energy ESRF_description="Energy" NAPItype="NX_FLOAT64" record="final" units="keV">${energy}</energy>
            <i0 ESRF_description="Incident flux" NAPItype="NX_FLOAT64" record="final" units="photons/s">${i0}</i0>
            <it ESRF_description="Transmitted flux" NAPItype="NX_FLOAT64" record="final" units="photons/s">${it}</it>
            <iC ESRF_description="Ionisation chamber flux" NAPItype="NX_FLOAT64" record="final" units="A">${iC}</iC>
            <vacuum ESRF_description="Vacuum values" NAPItype="NX_CHAR" record="final">${vacuum_values}</vacuum>
        </group>
    </group>
</group>

