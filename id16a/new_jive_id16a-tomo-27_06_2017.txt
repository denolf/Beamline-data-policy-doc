scanType
definition

InstrumentEnvironmentSensors_value = nano:20000/ID16NI/wago/kb/kb1_vmth, nano:20000/ID16NI/wago/kb/kb1_rx, nano:20000/ID16NI/wago/kb/kb1_hmth, nano:20000/ID16NI/wago/kb/kb1_tx, nano:20000/ID16NI/wago/kb/kb2_vmth, nano:20000/ID16NI/wago/kb/kb2_rx, nano:20000/ID16NI/wago/kb/kb2_hmth, nano:20000/ID16NI/wago/kb/kb2_tx

SamplePositioners_value = nano:20000/ID16NI/emotion_flexdc/srot/Position, nano:20000/ID16NI/emotion_pmd206_1/sx/Position, nano:20000/ID16NI/emotion_pmd206_1/sy/Position, nano:20000/ID16NI/tripod/sample/Zc, nano:20000/ID16NI/emotion_sp/spy/Position, nano:20000/ID16NI/emotion_spz/spz/Position, nano:20000/ID16NI/emotion_pmd206_2/su/Position, nano:20000/ID16NI/emotion_pmd206_2/sv/Position, nano:20000/ID16NI/tripod/sample/Pitch, nano:20000/ID16NI/tripod/sample/Roll, nano:20000/ID16NI/tripod/frame/Zc, nano:20000/ID16NI/tripod/frame/Pitch, nano:20000/ID16NI/tripod/frame/Roll




InstrumentSource_mode = orion:10000/fe/id/16/SR_Filling_Mode
InstrumentSource_current = orion:10000/fe/id/16/SR_Current
InstrumentMonochromator_energy = nano:20000/ID16NI/energy/master/Position
InstrumentMonochromator_name = nano:20000/ID16NI/energy/multilayer/positionId
InstrumentOpticsPositioners_value = nano:20000/ID16NI/slit/hpps-h/Position, nano:20000/ID16NI/slit/hpps-h/DiffPos, nano:20000/ID16NI/slit/hpps-v/Position, nano:20000/ID16NI/slit/hpps-v/DiffPos, nano:20000/ID16NI/motor/MLMY1/Position, nano:20000/ID16NI/motor/MLMY2/Position, nano:20000/ID16NI/motor/MLMZ/Position, nano:20000/ID16NI/emotion_e753/mlm-pz/Position, nano:20000/ID16NI/slit/hpcs-h/Position, nano:20000/ID16NI/slit/hpcs-h/DiffPos, nano:20000/ID16NI/beamviewer/bpm2/YagStatus, nano:20000/ID16NI/motor/BPM3Y/Position, nano:20000/ID16NI/motor/BPM3Z/Position, nano:20000/ID16NI/foil/bpm3/positionId, nano:20000/ID16NI/slit/hss-h/Position, nano:20000/ID16NI/slit/hss-h/DiffPos, nano:20000/ID16NI/beamviewer/bpm4/YagStatus, nano:20000/ID16NI/motor/BPM5Z/Position, nano:20000/ID16NI/foil/bpm5/positionId, nano:20000/ID16NI/slit/hpss-h/Position, nano:20000/ID16NI/slit/hpss-h/DiffPos, nano:20000/ID16NI/slit/hpss-v/Position, nano:20000/ID16NI/slit/hpss-v/DiffPos, nano:20000/ID16NI/inout/fshut/positionId, nano:20000/ID16NI/screen/bpm6/positionId, nano:20000/ID16NI/diodet/bpm6/positionId, nano:20000/ID16NI/slit/kbs-h/position, nano:20000/ID16NI/slit/kbs-h/diffpos, nano:20000/ID16NI/slit/kbs-v/position, nano:20000/ID16NI/slit/kbs-v/diffpos, nano:20000/ID16NI/emotion_pmd206_2/kby/Position, nano:20000/ID16NI/motor/PINHOLEY/Position, nano:20000/ID16NI/motor/PINHOLEZ/Position

InstrumentInsertionDevice_gap_value = orion:10000/id/id/16ni/U18-3C_GAP_Position, orion:10000/id/id/16ni/U18-3D_GAP_Position, orion:10000/id/id/16ni/U22-4C_GAP_Position, orion:10000/id/id/16ni/U22-4D_GAP_Position


SampleEnvironmentSensors_value = nano:20000/ID16NI/hpzdrift/hpz_tz/DriftCorrection, nano:20000/ID16NI/v-pen/111/Pressure
InstrumentAttenuatorPositioners_value = nano:20000/ID16NI/attenuator/1/positionId, nano:20000/ID16NI/attenuator/2/positionId, nano:20000/ID16NI/attenuator/3/positionId

detectors_positions = nano:20000/ID16NI/motor/CSAXSY/Position, nano:20000/ID16NI/motor/CSAXSZ/Position, nano:20000/ID16NI/motor/FLUO1Y/Position, nano:20000/ID16NI/motor/FLUO1Z/Position, nano:20000/ID16NI/motor/FLUO2Y/Position, nano:20000/ID16NI/motor/FLUO2Z/Position, nano:20000/ID16NI/motor/IMG1-FOCUS/Position, nano:20000/ID16NI/motor/IMG1-ROT/Position, nano:20000/ID16NI/motor/IMG1X/Position, nano:20000/ID16NI/motor/IMG1Y/Position, nano:20000/ID16NI/motor/IMG1Z/Position, nano:20000/ID16NI/motor/IMG2-CSWITCH/Position, nano:20000/ID16NI/motor/IMG2-FOCUS/Position, nano:20000/ID16NI/motor/IMG2C1-ROT/Position, nano:20000/ID16NI/motor/IMG2C2-ROT/Position, nano:20000/ID16NI/motor/IMG2X/Position, nano:20000/ID16NI/motor/IMG2Y/Position, nano:20000/ID16NI/motor/IMG2Z/Position, nano:20000/ID16NI/motor/VLM-FOCUS/Position, nano:20000/ID16NI/motor/VLM-LAT2/Position, nano:20000/ID16NI/motor/VLMY/Position, nano:20000/ID16NI/motor/VLMZ/Position, nano:20000/ID16NI/microscopecontrast/vlm/positionId

SCAN_N = nano:20000/ID16NI/spec/zaptomo/SCAN_N

TOMO_FTOMO_PAR = nano:20000/ID16NI/spec/zaptomo/FTOMO_PAR
TOMO_refN = nano:20000/ID16NI/spec/zaptomo/REF_N
TOMO_ccdtime = nano:20000/ID16NI/spec/zaptomo/TOMO_EXPTIME
TOMO_refOn = nano:20000/ID16NI/spec/zaptomo/TOMO_N
TOMO_yStep = nano:20000/ID16NI/spec/zaptomo/Y_STEP
TOMO_sx0 = nano:20000/ID16NI/sample/focus_position/position
TOMO_sourceSampleDistance
TOMO_detectorDistance
TOMO_darkN = nano:20000/ID16NI/spec/zaptomo/DARK_N
TOMO_i0 = nano:20000/ID16NI/keithley/bpm1/ReadData
TOMO_it = nano:20000/ID16NI/keithley/it/ReadData
TOMO_vacuumValues = nano:20000/ID16NI/v-pen/111/Pressure
TOMO_pixelSize = nano:20000/ID16NI/ImagePixelSize/frelon2/pixelWidth
TOMO_zStep = nano:20000/ID16NI/spec/zaptomo/Z_STEP
TOMO_refOn = nano:20000/ID16NI/spec/zaptomo/REF_ON


PTYCHO_Axis1_range 
PTYCHO_Axis2_range
PTYCHO_beamSize
PTYCHO_countTime= nano:20000/ID16NI/spec/zaptomo/TOMO_EXPTIME
PTYCHO_focusToDetectorDistance
PTYCHO_parameters = nano:20000/ID16NI/spec/zaptomo/NFP_PAR
PTYCHO_propagation
PTYCHO_refN =  nano:20000/ID16NI/spec/zaptomo/REF_N
PTYCHO_stepSize
PTYCHO_darkN = nano:20000/ID16NI/spec/zaptomo/DARK_N


FLUO_pixelSize
FLUO_dwellTime
FLUO_scanDim1
FLUO_scanDim2
FLUO_scanRange1
FLUO_scanRange2
FLUO_scanAxis1
FLUO_scanAxis2


