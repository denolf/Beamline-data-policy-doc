optics_sensors_values = nano:20000/ID16NI/wago/kb/kb1_vmth, nano:20000/ID16NI/wago/kb/kb1_rx, nano:20000/ID16NI/wago/kb/kb1_hmth, nano:20000/ID16NI/wago/kb/kb1_tx, nano:20000/ID16NI/wago/kb/kb2_vmth, nano:20000/ID16NI/wago/kb/kb2_rx, nano:20000/ID16NI/wago/kb/kb2_hmth, nano:20000/ID16NI/wago/kb/kb2_tx
sample_positions = nano:20000/ID16NI/emotion_flexdc/srot/Position, nano:20000/ID16NI/emotion_pmd206_1/sx/Position, nano:20000/ID16NI/emotion_pmd206_1/sy/Position, nano:20000/ID16NI/tripod/sample/Zc, nano:20000/ID16NI/emotion_sp/spy/Position, nano:20000/ID16NI/emotion_spz/spz/Position, nano:20000/ID16NI/emotion_pmd206_2/su/Position, nano:20000/ID16NI/emotion_pmd206_2/sv/Position, nano:20000/ID16NI/tripod/sample/Pitch, nano:20000/ID16NI/tripod/sample/Roll, nano:20000/ID16NI/tripod/frame/Zc, nano:20000/ID16NI/tripod/frame/Pitch, nano:20000/ID16NI/tripod/frame/Roll
optics_positions = nano:20000/ID16NI/slit/hpps-h/Position, nano:20000/ID16NI/slit/hpps-h/DiffPos, nano:20000/ID16NI/slit/hpps-v/Position, nano:20000/ID16NI/slit/hpps-v/DiffPos, nano:20000/ID16NI/motor/MLMY1/Position, nano:20000/ID16NI/motor/MLMY2/Position, nano:20000/ID16NI/motor/MLMZ/Position, nano:20000/ID16NI/emotion_e753/mlm-pz/Position, nano:20000/ID16NI/slit/hpcs-h/Position, nano:20000/ID16NI/slit/hpcs-h/DiffPos, nano:20000/ID16NI/beamviewer/bpm2/YagStatus, nano:20000/ID16NI/motor/BPM3Y/Position, nano:20000/ID16NI/motor/BPM3Z/Position, nano:20000/ID16NI/foil/bpm3/positionId, nano:20000/ID16NI/slit/hss-h/Position, nano:20000/ID16NI/slit/hss-h/DiffPos, nano:20000/ID16NI/beamviewer/bpm4/YagStatus, nano:20000/ID16NI/motor/BPM5Z/Position, nano:20000/ID16NI/foil/bpm5/positionId, nano:20000/ID16NI/slit/hpss-h/Position, nano:20000/ID16NI/slit/hpss-h/DiffPos, nano:20000/ID16NI/slit/hpss-v/Position, nano:20000/ID16NI/slit/hpss-v/DiffPos, nano:20000/ID16NI/inout/fshut/positionId, nano:20000/ID16NI/screen/bpm6/positionId, nano:20000/ID16NI/diodet/bpm6/positionId, nano:20000/ID16NI/slit/kbs-h/position, nano:20000/ID16NI/slit/kbs-h/diffpos, nano:20000/ID16NI/slit/kbs-v/position, nano:20000/ID16NI/slit/kbs-v/diffpos, nano:20000/ID16NI/emotion_pmd206_2/kby/Position, nano:20000/ID16NI/motor/PINHOLEY/Position, nano:20000/ID16NI/motor/PINHOLEZ/Position
vacuum_values = nano:20000/ID16NI/v-pen/111/Pressure
energy = nano:20000/ID16NI/energy/master/Position
monochromatorName = nano:20000/ID16NI/energy/multilayer/positionId
sample_sensors_values = nano:20000/ID16NI/hpzdrift/hpz_tz/DriftCorrection
insertionDeviceGap = orion:10000/id/id/16ni/U18-3C_GAP_Position, orion:10000/id/id/16ni/U18-3D_GAP_Position, orion:10000/id/id/16ni/U22-4C_GAP_Position, orion:10000/id/id/16ni/U22-4D_GAP_Position
i0 = nano:20000/ID16NI/keithley/bpm1/ReadData
it = nano:20000/ID16NI/keithley/it/ReadData
filter = nano:20000/ID16NI/attenuator/1/positionId, nano:20000/ID16NI/attenuator/2/positionId, nano:20000/ID16NI/attenuator/3/positionId
machineMode = orion:10000/fe/id/16/SR_Filling_Mode
detectors_positions = nano:20000/ID16NI/motor/CSAXSY/Position, nano:20000/ID16NI/motor/CSAXSZ/Position, nano:20000/ID16NI/motor/FLUO1Y/Position, nano:20000/ID16NI/motor/FLUO1Z/Position, nano:20000/ID16NI/motor/FLUO2Y/Position, nano:20000/ID16NI/motor/FLUO2Z/Position, nano:20000/ID16NI/motor/IMG1-FOCUS/Position, nano:20000/ID16NI/motor/IMG1-ROT/Position, nano:20000/ID16NI/motor/IMG1X/Position, nano:20000/ID16NI/motor/IMG1Y/Position, nano:20000/ID16NI/motor/IMG1Z/Position, nano:20000/ID16NI/motor/IMG2-CSWITCH/Position, nano:20000/ID16NI/motor/IMG2-FOCUS/Position, nano:20000/ID16NI/motor/IMG2C1-ROT/Position, nano:20000/ID16NI/motor/IMG2C2-ROT/Position, nano:20000/ID16NI/motor/IMG2X/Position, nano:20000/ID16NI/motor/IMG2Y/Position, nano:20000/ID16NI/motor/IMG2Z/Position, nano:20000/ID16NI/motor/VLM-FOCUS/Position, nano:20000/ID16NI/motor/VLM-LAT2/Position, nano:20000/ID16NI/motor/VLMY/Position, nano:20000/ID16NI/motor/VLMZ/Position, nano:20000/ID16NI/microscopecontrast/vlm/positionId
SCAN_N = nano:20000/ID16NI/spec/zaptomo/SCAN_N
sx0 = nano:20000/ID16NI/sample/focus_position/position
SR_Current = orion:10000/fe/id/16/SR_Current
scanType
pixelSize
dwellTime
scanDim_1
scanDim_2
scanRange_1
scanRange_2
scanAxis_1
scanAxis_2
