ID30-A1
============

### Connection
```
capek:~ % id
uid=2034(blissadm) gid=125(bliss) groups=125(bliss),20(dialout),44(video)
```

### Python Client

There is a python client which basically is a class that will interface with the MetadataManager and MetaExperiment device. It has as requeriment PyTango version 8.1.7 at least.

Example of use:
```
metadataManagerName = 'id30a1/metadata/ingest'
metaExperimentName  = 'id30a1/metadata/experiment'
client = MetadataManagerClient(metadataManagerName, metaExperimentName)

client.start('/tmp/metadata', 'mx415', 'sample2', 'dataset3')
client.printStatus()
client.end()
```

Client can be found on:
```
http://gitlab.esrf.fr/demariaa/Beamline-data-policy-doc/blob/master/python/MedataManagerClient.py
```

### Granularity

It was decided that the *granularity* will be *for each data collection*. There was a debate between sample or data collection but the drawback of collecting by sample is that it may be a large number of data collections, a lot of time of experiment which some parameters (resolution, energy, etc..) could eventually dramatically change.



### MxCube and Data Policy

MxCube is the control software for most of the MX beamlines. It manages the webservices calls to ISPyB (lims) internally. It was, then, the natural place to write the code to call to the tango devices. Some comments pointed out that REDIS could be a more appropiate place because could be reuse in other beamlines that they don't belong to the MX group but that use REDIS

### Raw data

Apparently there is not raw data with the motors positions, etc.. *to be clarified*

