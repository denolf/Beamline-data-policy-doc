Metadata
============

## Root
| Parameter   | Description            | Mandatory | Automatically filled | Example |
| --------    | ----------------------------------------------------------------------------------------------- | --- | ---------| --------|
| proposal    | proposal code and number                                                                        | x |            | MX0003               |
| folder_path | folder where output of the data collection can be found. Files in this folder will be archived  | x |            | /data/id30a1/inhouse |
| start_time | Start time of the data collection                                                                | x | Yes        |                      |
| end_time | End time of the data collection                                                                    | x | Yes        |                      |
| title | Data set name. This should be unique per proposal and sample                                          | x | Yes        | Sample1_15mg_60C     |
| files | Files in the folder_path directory and that will be archived depeding on the filters                  | x | Yes        |     |

## Instrument

### Source

| Parameter   | Description            | Mandatory | Automatically filled | Example |
| --------    | ----------------------------------------------------------------------------------------------- | --- | ---------| --------|
| InstrumentSource_mode |  Single Bunch or Multi Bunch:              |   |            |      |
| InstrumentSource_current | Accelerator, X-ray tube, or storage ring current             |   |            |     |


### Insertion Device

| Parameter   | Description            | Mandatory | Automatically filled | Example |
| --------    | ----------------------------------------------------------------------------------------------- | --- | ---------| --------|
| InstrumentInsertionDevice_gap_name | White space separated list of names of the motors for GAP                |   |            | U32a U42b U42c     |
| InstrumentInsertionDevice_gap_value | White space separated list of devices of the motors for GAP             |   |            | orion:10000/id/id/21/U32a_GAP_Position,orion:10000/id/id/21/U42b_GAP_Position, orion:10000/id/id/21/U42c_GAP_Position     |
| InstrumentInsertionDevice_taper_name | White space separated list of names of the motors for TAPER            |   |            | U32a U42b U42c     |
| InstrumentInsertionDevice_taper_value | White space separated list of devicesof the motors for TAPER          |   |            | orion:10000/id/id/21/U32a_TAPER_Position,orion:10000/id/id/21/U42b_TAPER_Position, orion:10000/id/id/21/U42c_TAPER_Position     |

### Slits

#### Primary Slit
| Parameter   | Description            | Mandatory | Automatically filled | Example |
| --------    | ----------------------------------------------------------------------------------------------- | --- | ---------| --------|
| InstrumentPrimarySlit_name |       |   |            |      |
| InstrumentPrimarySlit_vertical_gap |       |   |            |      |
| InstrumentPrimarySlit_vertical_offset |       |   |            |      |
| InstrumentPrimarySlit_horizontal_gap |       |   |            |      |
| InstrumentPrimarySlit_horizontal_offset |       |   |            |      |
| InstrumentSlitsPrimary_blade_up |       |   |            |      |
| InstrumentSlitsPrimary_blade_down |       |   |            |      |
| InstrumentPrimarySlit_horizontal_front |       |   |            |      |
| InstrumentPrimarySlit_horizontal_back |       |   |            |      |

#### Secondary Slit
| Parameter   | Description            | Mandatory | Automatically filled | Example |
| --------    | ----------------------------------------------------------------------------------------------- | --- | ---------| --------|
| InstrumentSecondarySlit_name |       |   |            |      |
| InstrumentSecondarySlit_vertical_gap |       |   |            |      |
| InstrumentSecondarySlit_vertical_offset |       |   |            |      |
| InstrumentSecondarySlit_horizontal_gap |       |   |            |      |
| InstrumentSecondarySlit_horizontal_offset |       |   |            |      |
| InstrumentSlitsSecondary_blade_up |       |   |            |      |
| InstrumentSlitsSecondary_blade_down |       |   |            |      |
| InstrumentSecondarySlit_horizontal_front |       |   |            |      |
| InstrumentSecondarySlit_horizontal_back |       |   |            |      |


#### Slits
| Parameter   | Description            | Mandatory | Automatically filled | Example |
| --------    | ----------------------------------------------------------------------------------------------- | --- | ---------| --------|
| InstrumentSlits_name |       |   |            |      |
| InstrumentSlits_vertical_gap |       |   |            |      |
| InstrumentSlits_vertical_offset |       |   |            |      |
| InstrumentSlits_horizontal_gap |       |   |            |      |
| InstrumentSlits_horizontal_offset |       |   |            |      |
| InstrumentSlits_blade_up |       |   |            |      |
| InstrumentSlits_blade_down |       |   |            |      |
| InstrumentSlits_horizontal_front |       |   |            |      |
| InstrumentSlits_horizontal_back |       |   |            |      |



