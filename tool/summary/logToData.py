import os

def getFiles():
	# Place on current Path
	search_dir = os.path.dirname(os.path.realpath(__file__))
	os.chdir(search_dir)

	# List all files
	files = filter(os.path.isfile, os.listdir(search_dir))
	files = [os.path.join(search_dir, f) for f in files] # add path to each file

	# Sort by creation
	files.sort(key=lambda x: os.path.getmtime(x))

	# Filter by name. File contains summary.log
	return [x for x in files if "summary.log" in x]

def parseFile(file):
	with open(fname) as f:
    		content = f.readlines()

def main():
	files = getFiles()
	for file in files:
		print file

if __name__ == "__main__":
	main()
