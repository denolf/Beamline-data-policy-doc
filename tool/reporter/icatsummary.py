#! /usr/bin/python
#
# Search the ICAT for all entity types and report the number of
# objects found for each type.
#

from __future__ import print_function
import icat
import icat.config
import logging
import graypy
from time import gmtime, strftime
import json
import os

graylogServer = 'graylog-dau.esrf.fr'
graylogPort = 12201
reportFilePath = '/users/demariaa/Software/metadata/Beamline-data-policy-doc/tool/reporter/report.csv'

# Suds Logger
logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.CRITICAL)

# Graylog Logger
my_logger = logging.getLogger('test_logger')
my_logger.setLevel(logging.DEBUG)

handler = graypy.GELFHandler(graylogServer, graylogPort)
my_logger.addHandler(handler)


# ICAT configuration
conf = icat.config.Config().getconfig()

client = icat.Client(conf.url, **conf.client_kwargs)
client.login(conf.auth, conf.credentials)


print("User: %s" % client.getUserName())
print()

previousResult = None
# If report.csv exists then read it
if os.path.exists(reportFilePath):
    file = open(reportFilePath, "r") 
    previousResult = json.load(file)
    file.close()     
   

result = {}
entitycolwidth = 24
print("%-*s   %s" % (entitycolwidth, "Entity", "count"))
print("-" * (entitycolwidth + 3 + 5))
for entityname in client.getEntityNames():
    if entityname == "Log":
        continue
    try:
        res = client.search("SELECT COUNT(e) FROM %s e" % entityname)[0]
    except icat.exception.ICATPrivilegesError:
        # ICAT 4.2.* raises a PrivilegesError if there are entities
        # matching the search but the user has no read permission to
        # any of them.  ICAT 4.3.* returns an empty list.  See ICAT
        # Issue 120.
        res = 0
    except IndexError:
        # ref. ICAT issue 131
        res = 0
    print("%-*s : %d" % (entitycolwidth, entityname, res))
    result[entityname]=int(res)

# Writting latest result into file
f = open(reportFilePath, 'w')
f.write(json.dumps(result, indent=4, sort_keys=True)) 
f.close() 

# Substracting results
if previousResult is not None:
    for key in previousResult:
        result[key] = str(int(result[key]) - int(previousResult[key]))
  
# Creating message to graylog
message = "ICAT stats. Time=" + strftime("%Y-%m-%d %H:%M:%S", gmtime()) + " "
for my_var in result:
    message = message + str(my_var) + '=' +  str(result[my_var]) + " "
my_logger.debug(message)




