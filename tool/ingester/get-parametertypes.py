#! /usr/bin/python
#
# Create some parameter types, (actually just one for testing atm).
#

from __future__ import print_function
import icat
import icat.config
import sys
import logging
from sets import Set
import xml.etree.ElementTree as ET
import re

logging.basicConfig(level=logging.INFO)
#logging.getLogger('suds.client').setLevel(logging.DEBUG)

conf = icat.config.Config().getconfig()

client = icat.Client(conf.url, **conf.client_kwargs)
client.login(conf.auth, conf.credentials)



# ------------------------------------------------------------
# Get parameters type from ICAT we need later on
# ------------------------------------------------------------

parametersType = client.search("ParameterType")

parametersList = []
for parameterType in parametersType:
	parametersList.append(parameterType.name)

dictionary = (set(parametersList))


# ------------------------------------------------------------
# Get parameters from xml file
# ------------------------------------------------------------

tree = ET.parse('../../hdf5_cfg.xml') 
text = (ET.tostring(tree.getroot(), encoding='utf8', method='text'))

noBlankLines = filter(lambda x: not re.match(r'^\s*$', x), text)
attributeList = noBlankLines.replace("$", "\n").replace("{", "").replace("}", "").split("\n")[1:]

# -------------------
# Removing duplicates
# -------------------
attributeList = list(set(attributeList))

# ------------------------------------------------------------
# This compares the list of parameters of the database and the list of parameters of the xml
# ------------------------------------------------------------
parametertype_data = []

for attribute in attributeList:
	if attribute in dictionary:
		print(attribute + " already exists")
	else:
		parametertype_data.append( {
			'name': attribute,
			'units' : 'NA',
			'unitsFullName' : '',
			'valueType': "STRING",
		})

	
# ------------------------------------------------------------
# Storing parameters type on ICAT
# ------------------------------------------------------------
hzb = client.assertedSearch("Facility[name='ESRF']")[0]
parametertypes = []
for pdata in parametertype_data:
    print("ParameterType: creating '%s' ..." % pdata['name'])
    parametertype = client.new("parameterType")
    parametertype.name = pdata['name']
    parametertype.units = pdata['units']
    parametertype.unitsFullName = pdata['unitsFullName']
    parametertype.valueType = pdata['valueType']
    parametertype.applicableToDatafile = True
    parametertype.applicableToDataset = True
    parametertype.applicableToSample = True
    parametertype.applicableToInvestigation = True
    parametertype.facility = hzb
    parametertypes.append(parametertype)
client.createMany(parametertypes)
