## Ingester

It reads the parameters from the configuration file and if they don't exist then it stores as parameterType on ICAT.

### Run

```
python  get-parametertypes.py -s ovm --no-check-certificate -c /users/demariaa/Downloads/python-icat-0.12.0/doc/examples/icat.cfg --https-proxy ""
```

### Configuration

Configuration is read from /users/demariaa/Downloads/python-icat-0.12.0/doc/examples/icat.cfg

Example of a section:

```

[ovm]
url = https://ovm-icat2:8181/ICATService/ICAT?wsdl
auth = db
username = root
password = *****
idsurl = https://ovm-icat2:8181/ids
# uncomment, if your server does not have a trusted certificate
checkCert = No

```

