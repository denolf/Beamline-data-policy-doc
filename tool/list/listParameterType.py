#! /usr/bin/python
#
# Search the ICAT for all entity types and report the number of
# objects found for each type.
#

from __future__ import print_function
import icat
import icat.config
import logging
import sys
import csv
from distutils.util import strtobool

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.CRITICAL)

conf = icat.config.Config().getconfig()
client = icat.Client(conf.url, **conf.client_kwargs)
client.login(conf.auth, conf.credentials)

try:
    parameterTypeList = client.search("SELECT (p) FROM ParameterType p"  )
    parameterTypeList = sorted(parameterTypeList, key=lambda k: k.name)  
    for parameter in parameterTypeList:
        print("[%s] Units:(%s), Description: %s"% (parameter.name, parameter.units, parameter.description))    
    
except icat.exception.ICATPrivilegesError:      
    print("[ERROR] A error was produced")


